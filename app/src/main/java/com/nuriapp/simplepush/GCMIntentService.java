package com.nuriapp.simplepush;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class GCMIntentService extends GCMBaseIntentService {
    private static final String tag = "GCMIntentService";
    public static final String PROJECT_ID = "434004627225";
    //구글 api 페이지 주소 [https://code.google.com/apis/console/#project:긴 번호]
   //#project: 이후의 숫자가 위의 PROJECT_ID 값에 해당한다

    //public 기본 생성자를 무조건 만들어야 한다.
    public GCMIntentService(){ this(PROJECT_ID); }
   
    public GCMIntentService(String project_id) { super(project_id); }
 
    /** 푸시로 받은 메시지 */
    @SuppressLint("NewApi")
	@Override
    protected void onMessage(Context context, Intent intent) {
        Bundle b = intent.getExtras();

        HashMap<String, String> map = new HashMap<String, String>();
        String code = null;
        String title = null;
        String content = null;

        Iterator<String> iterator = b.keySet().iterator();
        while(iterator.hasNext()) {
            String key = iterator.next();
            String value = null;
            try {
                value = URLDecoder.decode(b.get(key).toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Log.d(tag, "onMessage. key: "+key+", value: "+value);
            try {
                map.put(key, value);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        Intent main_intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, main_intent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification noti = null;
        String time;
        SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        if(map.get("code") != null) {
            code = ((String)map.get("code"));
            time = map.get("goWorkTime").substring(0,19);
            // 출근
            if(code.equals("C001")){
                MainActivity.userName = map.get("empName");

                title = String.format("%s님 출근!!", map.get("empName"));
                String name = map.get("empName");
                content = String.format("%s 에 출근하셨습니다.", time);

                noti = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.push_icon)
                        .setTicker(title)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pIntent)
                        .build();

                if("01027922787".equals(map.get("phoneNumber").toString())){
                    editor.putString("INTIME", time);
                    editor.putString("textMain", Html.fromHtml("<FONT color=#f07b14>"+name+"</FONT>")+"님 "+content);
                }
            }
            // 퇴근
            else if(code.equals("C002")){
                MainActivity.userName = map.get("empName");

                title = String.format("%s님 퇴근!!", map.get("empName"));
                String name = map.get("empName");
                content = String.format("%s 에 퇴근하셨습니다.", time);

                noti = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.push_icon)
                        .setTicker(title)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pIntent)
                        .build();

                if("01027922787".equals(map.get("phoneNumber").toString())) {
                    editor.putString("OUTTIME", time);
                    editor.putString("textMain", Html.fromHtml("<FONT color=#f07b14>" + name + "</FONT>") + "님 " + content);
                }
            }
        } else {
            noti = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.push_icon)
                    .setTicker(title)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pIntent)
                    .build();
        }

        editor.commit();
        
        noti.defaults |= Notification.DEFAULT_SOUND;
        //noti.defaults |= Notification.DEFAULT_ALL;
        noti.flags |= Notification.FLAG_AUTO_CANCEL;	
        
        
		NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);		
		nm.notify(0, noti);
        if(NuriBeacon.ListActivity != null){
            ListActivity listActivity = new ListActivity();
            Log.i("GCMIntentService", "*************** 빠밤 전!");
            listActivity.refreshList();
            Log.i("GCMIntentService", "*************** 빠밤 후!");
        }
    }

    /**에러 발생시*/
    @Override
    protected void onError(Context context, String errorId) {
        Log.d(tag, "onError. errorId : "+errorId);
    }
 
    /**단말에서 GCM 서비스 등록 했을 때 등록 id를 받는다*/
    @Override
    protected void onRegistered(Context context, String regId) {
        Log.d(tag, "onRegistered. regId : "+regId);
    }

    /**단말에서 GCM 서비스 등록 해지를 하면 해지된 등록 id를 받는다*/
    @Override
    protected void onUnregistered(Context context, String regId) {
        Log.d(tag, "onUnregistered. regId : " + regId);
    }

    public class MyAsyncTask extends AsyncTask<String, Integer, SimpleAdapter> {
        @Override
        protected void onPreExecute() {
            Log.i("GCMIntentService", "*************** onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected SimpleAdapter doInBackground(String... string) {
            Log.i("GCMIntentService", "*************** doInBackground");
            SimpleAdapter simpleAdapter = null;
            RequestPush requestPush = new RequestPush();
            try{
                JSONArray json = requestPush.selectList();
                try {
                    JSONObject jsonObject;
                    ArrayList<HashMap<String, String>> workList = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < json.length(); i++) {
                        jsonObject = json.getJSONObject(i);
                        String empName = jsonObject.getString("EMP_NAME");
                        String goWorkTime = jsonObject.getString("GOWORK_TIME");
                        String goWorkType = jsonObject.getString("GOWORK_TYPE");
                        String phoneNumber = jsonObject.getString("PHONE_NUMBER");
                        Log.i("**************", empName + ", " + goWorkType + ", " + goWorkTime + ", " + phoneNumber);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("empName", empName);
                        map.put("goWorkTime", goWorkTime.substring(0, 19));
                        map.put("goWorkType", "1".equals(goWorkType) ? "출근" : "퇴근");
                        map.put("phoneNumber", phoneNumber);
                        workList.add(map);
                    }

                    simpleAdapter = new SimpleAdapter(getApplicationContext(), workList, R.layout.item_list, new String[]{"empName", "goWorkTime", "goWorkType", "phoneNumber"}, new int[]{R.id.name, R.id.workTime, R.id.workType, R.id.phoneNumber});
                    //vWorkList.setAdapter(simpleAdapter);
                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
            return simpleAdapter;
        }

        @Override
        protected void onPostExecute(SimpleAdapter sa) {
            Log.i("GCMIntentService", "*************** onPostExecute");
            super.onPostExecute(sa);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}