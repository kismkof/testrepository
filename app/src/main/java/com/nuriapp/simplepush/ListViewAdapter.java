package com.nuriapp.simplepush;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mkkim on 2016-01-26.
 */
public class ListViewAdapter extends ArrayAdapter<ListItem> {
    Context context;
    List<ListItem> itemList;
    int layoutResID;

    public ListViewAdapter(Context context, int layoutResourceID, List<ListItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.itemList = listItems;
        this.layoutResID = layoutResourceID;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder holder;
        View view = convertView;

        if(view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            holder = new ListItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.workTime = (TextView) view.findViewById(R.id.workTime);
            holder.workType = (TextView) view.findViewById(R.id.workType);
            holder.phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);

            view.setTag(holder);
        } else {
            holder = (ListItemHolder) view.getTag();
        }

        ListItem listItem = (ListItem) this.itemList.get(position);

        holder.name.setText(listItem.getName());
        holder.workTime.setText(listItem.getWorkTime());
        holder.workType.setText(listItem.getWorkType());
        holder.phoneNumber.setText(listItem.getPhoneNumber());
        return view;
    };

    public static class ListItemHolder {
        TextView name;
        TextView workTime;
        TextView workType;
        TextView phoneNumber;
    }
}
