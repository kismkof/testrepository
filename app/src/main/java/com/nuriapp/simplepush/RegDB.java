package com.nuriapp.simplepush;

import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

public class RegDB {
	
	// DB 등록
	/*public void addRegId(String regId) throws SQLException, ClientProtocolException, IOException{

		String device_serial = getDeviceSerialNumber();

		HttpClient client = new DefaultHttpClient();
		String url = MainActivity.APP_URL.substring(0,MainActivity.APP_URL.lastIndexOf('/'));

		String postURL = url+"/keyInsert.do";

		Log.i("APP_URL", postURL);

		HttpPost post = new HttpPost(postURL);

		List params = new ArrayList();
		//params.add(new BasicNameValuePair("type", "Android"));
		params.add(new BasicNameValuePair("uuid", device_serial));
		params.add(new BasicNameValuePair("regId", regId));

		UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, "utf-8");
		post.setEntity(ent);

		HttpResponse responsePost = client.execute(post);
		HttpEntity reEntity = responsePost.getEntity();

		if(reEntity !=null){
			Log.w("RESPONSE", EntityUtils.toString(reEntity));
		}
	}*/

	// DB 등록
	public void addRegId(String regId) throws SQLException, IOException{
        Log.i("RegDB", "*************** addRegId");
		String device_serial = getDeviceSerialNumber();

		String tmp = MainActivity.APP_URL.substring(0, MainActivity.APP_URL.lastIndexOf('/'));
		String postURL = tmp+"/keyInsert.do";
		String param = "phoneNumber="+"01027922787"+"&regKey="+regId;

		URL url = new URL(postURL);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestProperty("Content_Type", "application/x-www-form-urlencoded");
		connection.setRequestMethod("POST");

		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.connect();

		OutputStream opstrm = connection.getOutputStream();
		opstrm.write(param.getBytes());
		opstrm.flush();
		opstrm.close();

		String buffer;
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		while((buffer = in.readLine()) != null){
            System.out.println(buffer);
		}
		in.close();
	}

    public void insertAttendence(String regId, String code) throws SQLException, IOException{
        Log.i("RegDB", "*************** insertAttendence"+code);
        String device_serial = getDeviceSerialNumber();

        String tmp = MainActivity.APP_URL.substring(0, MainActivity.APP_URL.lastIndexOf('/'));
        String postURL = tmp+"/goWork.do";
        String param = "phoneNumber="+"01027922787"+"&regKey="+regId+"&code="+code;

        URL url = new URL(postURL);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestProperty("Content_Type", "application/x-www-form-urlencoded");
        connection.setRequestMethod("POST");

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();
        OutputStream opstrm = connection.getOutputStream();
        opstrm.write(param.getBytes());
        opstrm.flush();
        opstrm.close();

        String buffer;
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        while((buffer = in.readLine()) != null){
        }
        in.close();
    }
	
	// 기기 시리얼 번호 받기
	public static String getDeviceSerialNumber() {
	  try {
	    return (String) Build.class.getField("SERIAL").get(null);
	  } catch (Exception ignored) {
	    return null;
	  }
	}
}
