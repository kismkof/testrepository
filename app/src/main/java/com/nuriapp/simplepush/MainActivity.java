package com.nuriapp.simplepush;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.ccbeacon.beacon.Beacon;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final String KEY_UUID = "uuid";
    public static final String KEY_MAJOR = "major";
    public static final String KEY_MINOR = "minor";
    public static String userName;
    ArrayList<HashMap<String, String>> BeaconsList = new ArrayList<HashMap<String, String>>();

    public static final String APP_URL = "http://192.168.0.150:8080/";
    private int count = 0;
    TextView attendenceText;
    Button btnListActivity,btnStopSrv;
    private void log(String string) {
        Log.i(TAG, "*************** " + string);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity", "********** onCreate");
        setContentView(R.layout.activity_main);

        final Intent intent = new Intent(this, NuriconDemonService.class);

        attendenceText = (TextView) findViewById(R.id.attendenceText);
        SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
        Log.i("MainActivity", "textMain: " + pref.getString("textMain", ""));
        attendenceText.setText(pref.getString("textMain", ""));
        btnStopSrv = (Button) findViewById(R.id.btnStopSrv);
        btnListActivity = (Button) findViewById(R.id.btnListActivity);
        btnStopSrv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(intent);
                //finishMain();
            }
        });
        btnListActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log("call ListActivity!");
                Intent intentListActivity = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intentListActivity);
            }
        });

        startService(intent);

        log("onCreate step 1");
        ListThread listThread = new ListThread(true);
        log("listThread start!");
        listThread.start();
        log("onCreate step 2");
    }

    private class ListThread extends Thread {
        boolean running = false;
        ListThread(boolean b) {running = b;}

        @Override
        public void run() {
            log("in ListThread start with: "+running);
            if(running) {
                //listView.setAdapter(simpleAdapter);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void didFindBeaconInReagion(Beacon beacon, double distance) {
        Log.d("MainActivity", "didFindBeaconInReagion");
        AddNewBeaconProcess(beacon.getId1().toString(), beacon.getId2().toString(), beacon.getId3().toString(), distance);
    }

    public void AddNewBeaconProcess(final String uuid, final String major, final String minor, final double distance) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getData(uuid + "-" + major + "-" + "minor", uuid, major, minor, distance);
            }
        });
    }

    public void getData(String result, String uuid, String major, String minor, double distance) {
        HashMap<String, String> song = null;
        String _uuid, _major, _minor;
        boolean is_bankfind = false;
        double distan = distance;

        for (int i=0; i < BeaconsList.size(); i++) {
            song = BeaconsList.get(i);

            _uuid = song.get(KEY_UUID);
            _major = song.get(KEY_MAJOR);
            _minor = song.get(KEY_MINOR);

            if (_uuid.equals(uuid) && _major.equals(major) && _minor.equals(minor)) {
                is_bankfind = true;
                Log.i(TAG, String.format("%.3f", distan) + "m ");
                Log.i(TAG, "*************** count: "+count++);
                break;
            }
        }

        if ( !is_bankfind ) {
            Log.i("!is_bankfind", "UUID:"+uuid+"MAJOR:"+major+"MINOR:"+minor);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(KEY_UUID, uuid);
            map.put(KEY_MAJOR, major);
            map.put(KEY_MINOR, minor);
            BeaconsList.add(map);
        }
    }

    private final void finishMain() {
        this.finish();
    }
}
