package com.nuriapp.simplepush;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.ccbeacon.beacon.Beacon;
import org.ccbeacon.beacon.BeaconConsumer;
import org.ccbeacon.beacon.BeaconManager;
import org.ccbeacon.beacon.BeaconParser;
import org.ccbeacon.beacon.MonitorNotifier;
import org.ccbeacon.beacon.RangeNotifier;
import org.ccbeacon.beacon.Region;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Created by mkkim on 2016-01-18.
 */
public class NuriconDemonService extends Service implements BeaconConsumer{
    public static final String INTIME = "INTIME";
    public static final String OUTTIME = "OUTTIME";
    private static final String TAG = "NuriconDemonService";
    private static final String NURICON_UUID = "cc86b3c0-2bd1-413b-bf71-43e83c2f5bb1"; //+45057
    public BeaconManager beaconManager = null;
    private double distan;
    public MainActivity main = null;
    private String regId;

    private Handler mHandler, timerStartHandler, timerResetHander;
    private int timerState = 0;
    public void setTimerState(int i) {log("setTimerState");this.timerState = i;}
    public int getTimerState() {log("getTimerState");return this.timerState;}
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "********** onCreate");
        mHandler = new Handler();
        registerDevice();

        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().clear();
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        beaconManager.bind(this);
        beaconManager.setForegroundBetweenScanPeriod(500);

        main = new MainActivity();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "********** onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "*************** "+TAG+" destroyed");
        super.onDestroy();
        beaconManager.unbind(this);
        Log.d(TAG, "*************** " + TAG + " Service unbinded successfully!!!");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    Activity dActivity = new Activity();
    @Override
    public void onBeaconServiceConnect() {
        Log.d(TAG, "*************** onBeaconServiceConnect");
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    Log.d(TAG, "*************** beacons.size: " + beacons.size());

                    for (Beacon firstBeacon : beacons) {
                        distan = firstBeacon.getDistance();
                        Log.i(TAG, "*************** UUID: " + firstBeacon.getId1().toString() + ", MAJOR: " + firstBeacon.getId2() + ", MINOR: " + firstBeacon.getId3() + "Distant : " + String.valueOf(distan));

                        if (NURICON_UUID.equals(firstBeacon.getId1().toString())) {
                            if (!getPreference(INTIME).contains(getToday())) {
                                mHandler.post(new UtilRunnable("Toast", "출근 확인됨!"));
                                sendRequest("C001");
                            }
                        }

                        Date date = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
                        log(Integer.parseInt(sdf.format(date)) + "");

                        if(distan > 8 && Integer.parseInt(sdf.format(date)) > 180000) {
                            if (!getPreference(OUTTIME).contains(getToday())) {
                                mHandler.post(new UtilRunnable("Toast", "퇴근 확인됨!"));
                                sendRequest("C002");
                            }
                        }
                        /*if (main != null) {
                            main.didFindBeaconInReagion(firstBeacon, distan);
                        }*/
                    }
                }
            }
        });
        beaconManager.setMonitorNotifier(new MonitorNotifier() {

            @Override
            public void didEnterRegion(Region region) {
                /*log("didEnterRegion");
                if(NURICON_UUID.equals(region.getId1()) && getPreference().contains(getToday())){
                    mHandler.post(new UtilRunnable("김민기님 "+dateUtil("FULL")+"에 복귀"));
                }*/
            }

            @Override
            public void didExitRegion(Region region) {
                /*log("didExitRegion");
                if (getPreference(INTIME).contains(getToday())) {
                    mHandler.post(new UtilRunnable("김민기님 "+dateUtil("FULL")+"에 퇴근"));
                }*/
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                /*log("didDetermineStateForRegion. region: "+region.toString());
                SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if(INSIDE == i){
                    log("INSIDE");
                    mHandler.post(new UtilRunnable("Toast", "김민기님 " + dateUtil("FULL") + "에 복귀"));
                    editor.putString("existTimer", "1");
                    editor.commit();
                    log("INSIDE_existTimer_1");
                }else if(OUTSIDE == i){
                    log("OUTSIDE");
                    if("1".equals(pref.getString("existTimer", ""))){
                        mHandler.post(new UtilRunnable("Toast", "김민기님 " + dateUtil("FULL") + "에 퇴근"));
                        editor.putString("existTimer", "0");
                        editor.commit();
                        log("INSIDE_existTimer_0");

                        if(0 == getTimerState()){
                            log("existTimer_start!!!");
                            new Handler().post(new mRunnable(""));
                            log("existTimer_check!!!");
                        }

                    }
                }*/
            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
            beaconManager.startMonitoringBeaconsInRegion(new Region("myMonitoringUniqueId", null, null, null));
        } catch (RemoteException e) {
        }
    }

    public void registerDevice(){
        Log.d(TAG, "*************** registerDevice");
        RegisterThread registerObj = new RegisterThread();
        registerObj.start();
    }

    public void sendRequest(String string){
        Log.d(TAG, "*************** sendRequest");
        SendThread send = new SendThread(string);
        send.start();
    }

    class RegisterThread extends Thread{
        public void run(){
            try {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                regId = gcm.register(GCMIntentService.PROJECT_ID);

                Log.d("regId : " + regId, regId);

                RegDB db = new RegDB();
                db.addRegId(regId);
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    class SendThread extends Thread{
        String code = null;
        SendThread(String _code) {
            this.code = _code;
        }
        public void run(){
            try {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                regId = gcm.register(GCMIntentService.PROJECT_ID);

                /*Log.d("regId : 123123123" + regId, regId);*/
                System.out.println("regId : 123123123" + regId);
                RegDB regDB = new RegDB();
                regDB.insertAttendence(regId, code);
                /*RequestPush push = new RequestPush();
                push.sendPush(regId);*/
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }
    public String getToday(){
        Calendar c = Calendar.getInstance();
        String month = Integer.toString(c.get(Calendar.MONTH) + 1);
        month = month.length()==1?"0"+month:month;
        String today = c.get(Calendar.YEAR)+"-"+month+"-"+c.get(Calendar.DATE);
        return today;
    }

    public String dateUtil(String datetime){
        String value = "";
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        String month = Integer.toString(c.get(Calendar.MONTH) + 1);
        month = month.length()==1?"0"+month:month;
        int day = c.get(Calendar.DATE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        switch(datetime){
            case("FULL"):
                value = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
                break;
            case("DATE"):
                value = year+"-"+month+"-"+day;
                break;
            case("TIME"):
                value = hour+":"+minute+":"+second;
                break;
        }
        return value;
    }

    public String getPreference(String _time){
        SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
        String time = null;
        switch(_time){
            case(INTIME):
                time = pref.getString(INTIME,"");
                break;
            case(OUTTIME):
                time = pref.getString(OUTTIME,"");
                break;
        }
        return time;
    }

    public void setPrefOutOffice() {
        SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("", "");
    }

    private void log(String string) {
        Log.i(TAG, "*************** " + string);
    }

    private class UtilRunnable implements Runnable {


        private String type, strToast;
        UtilRunnable(String _type) {type = _type;}
        UtilRunnable(String _type,String _string){
            type = _type;
            strToast = _string;
        }

        @Override
        public void run() {
            log("UtilRunnable");
            switch(type){
                case("Toast"):
                    log(type);
                    Toast.makeText(getBaseContext(), strToast, Toast.LENGTH_SHORT).show();
                    break;
                case("TimerOn"):
                    log(type);
                    setTimerState(1);
                    break;
                case("TimerOff"):
                    log(type);
                    setTimerState(0);
                    break;
            }

        }
    }

    public class mRunnable implements Runnable {
        private String exist;
        mRunnable(String exist){this.exist = exist;}
        @Override
        public void run() {
            timerStartHandler = new Handler();
            timerStartHandler.post(new UtilRunnable("TimerOn"));
            SharedPreferences pref = getSharedPreferences("nuricon", MODE_PRIVATE);
            for(int i=0;i<30;i++){
                try {
                    System.out.println("mRunnable_existTimer_" + i);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("existTimer after mRunnable: " + pref.getString("existTimer", ""));
            if(!"1".equals(pref.getString("existTimer", ""))){
                log("mRunnable_existTimer_0");
                log("김민기님 " + dateUtil("FULL") + "에 퇴근");
                mHandler.post(new UtilRunnable("Toast","김민기님 " + dateUtil("FULL") + "에 퇴근"));
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("existTimer", "-1");
                editor.commit();
            }
            timerResetHander = new Handler();
            timerResetHander.post(new UtilRunnable("TimerOff"));
        }
    }
}
