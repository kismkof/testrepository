package com.nuriapp.simplepush;

/**
 * Created by mkkim on 2016-01-18.
 */
public class ListItem {
    String name;
    String workType;
    String workTime;
    String phoneNumber;

    public ListItem(String name, String workTime, String workType, String phoneNumber) {
        super();
        this.name = name;
        this.workType = workType;
        this.workTime = workTime;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
