package com.nuriapp.simplepush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by mkkim on 2016-01-19.
 */
public class PackageReceiver extends BroadcastReceiver {
    private static final String TAG = "PackageReceiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(Intent.ACTION_PACKAGE_ADDED.equals(action)) { // 앱 설치시 작동
            Log.d(TAG, "********** package installed successfully!!");
            Toast.makeText(context, "Nuricon 앱이 설치되었습니다.", Toast.LENGTH_LONG).show();
        } else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) { // 앱 삭제시 작동
            Log.d(TAG, "********** package removed successfully!!");
            Toast.makeText(context, "Nuricon 앱이 삭제되었습니다.", Toast.LENGTH_LONG).show();
        }
    }
}
