package com.nuriapp.simplepush;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;


public class RequestPush {

    public void sendPush(String regId) throws IOException {

        String phoneNumber = "01056975812";

        String tmp = MainActivity.APP_URL.substring(0, MainActivity.APP_URL.lastIndexOf('/'));
        String postURL = tmp+"/pushSend.do";
        String param = "phoneNumber="+phoneNumber+"&regKey="+regId+"&title="+"Nuricon 알림 메세지"+"&content="+"김민기님 출근 확인되었습니다.";

        URL url = new URL(postURL);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestProperty("Content_Type", "application/x-www-form-urlencoded");
        connection.setRequestMethod("POST");

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();

        OutputStream opstrm = connection.getOutputStream();
        opstrm.write(param.getBytes());
        opstrm.flush();
        opstrm.close();

        String buffer = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while((buffer = in.readLine()) != null){

        }
        in.close();
    }

    public JSONArray selectList() throws IOException {
        String tmp = MainActivity.APP_URL.substring(0, MainActivity.APP_URL.lastIndexOf('/'));
        String postURL = tmp+"/callGoWorkList.do";

        URL url = new URL(postURL);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestProperty("Content_Type", "application/x-www-form-urlencoded");
        connection.setRequestMethod("POST");

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();

        OutputStream opstrm = connection.getOutputStream();
        //opstrm.write(param.getBytes());
        opstrm.flush();
        opstrm.close();

        String buffer = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        JSONArray json = null;
        while((buffer = in.readLine()) != null){
            //Log.i("RequestPush", "*************** " + buffer);
            try {
                json = new JSONArray(buffer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        in.close();
        Log.i("RequestPush", "*************** selectList DONE!");
        return json;
    }
}
