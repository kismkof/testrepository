package com.nuriapp.simplepush;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mkkim on 2016-01-26.
 */
public class ListActivity extends AppCompatActivity {

    private static ListView vWorkList;
    ArrayList<HashMap<String, String>> workList;
    private ListViewAdapter listViewAdapter;
    JSONArray json = null;
    private static Context cListActivity;

    public void refreshList() {
        Log.i("ListActivity", "*************** refreshList. START");
        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute("","");
        Log.i("ListActivity", "*************** refreshList. DONE");
    }

    /* 리스트 업데이트를 위한 AsyncTask 클래스 생성 */
    public class MyAsyncTask extends AsyncTask<String, Integer, SimpleAdapter> {
        @Override
        protected void onPreExecute() {
            log("onPreExecute");
            super.onPreExecute();
        }

        @Override
        protected SimpleAdapter doInBackground(String... string) {
            log("doInBackground");
            SimpleAdapter simpleAdapter = null;
            RequestPush requestPush = new RequestPush();
            try{
                JSONArray json = requestPush.selectList();
                try {
                    JSONObject jsonObject;
                    workList = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < 10/*json.length()*/; i++) {
                        jsonObject = json.getJSONObject(i);
                        String empName = jsonObject.getString("EMP_NAME");
                        String goWorkTime = jsonObject.getString("GOWORK_TIME");
                        String goWorkType = jsonObject.getString("GOWORK_TYPE");
                        String phoneNumber = jsonObject.getString("PHONE_NUMBER");
                        Log.i("**************", empName + ", " + goWorkType + ", " + goWorkTime + ", " + phoneNumber);

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("empName", empName);
                        map.put("goWorkTime", goWorkTime.substring(0, 19));
                        map.put("goWorkType", "1".equals(goWorkType) ? "출근" : "퇴근");
                        map.put("phoneNumber", phoneNumber);
                        workList.add(map);
                    }

                    simpleAdapter = new SimpleAdapter(cListActivity, workList, R.layout.item_list, new String[]{"empName", "goWorkTime", "goWorkType", "phoneNumber"}, new int[]{R.id.name, R.id.workTime, R.id.workType, R.id.phoneNumber});
                    //vWorkList.setAdapter(simpleAdapter);
                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
            return simpleAdapter;
        }

        @Override
        protected void onPostExecute(SimpleAdapter sa) {
            log("onPostExecute");
            super.onPostExecute(sa);
            vWorkList.setAdapter(sa);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("onCreate");
        cListActivity = getApplicationContext();
        NuriBeacon.ListActivity = ListActivity.this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        vWorkList = (ListView)findViewById(R.id.vWorkList);
        refreshList();
        //selectList("");
        Button btnRequestPush = (Button) findViewById(R.id.btnRequestPush);
        Button btnCallMain = (Button) findViewById(R.id.btnCallMain);
        btnRequestPush.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                log("new push requested!");

            }
        });
        btnCallMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                log("call MainActivity!");
                finish();
            }
        });
    }

    public void selectList(String string){
        log("selectList");
        SelectThread selectList = new SelectThread(string);
        selectList.start();
    }
    class SelectThread extends Thread {
        SelectThread(String string){}
        public void run() {
            log("SelectThread. run()");

            RequestPush requestPush = new RequestPush();
            try {
                json = requestPush.selectList();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject;
                            workList = new ArrayList<HashMap<String, String>>();
                            for (int i = 0; i < 10/*json.length()*/; i++) {
                                jsonObject = json.getJSONObject(i);
                                String empName = jsonObject.getString("EMP_NAME");
                                String goWorkTime = jsonObject.getString("GOWORK_TIME");
                                String goWorkType = jsonObject.getString("GOWORK_TYPE");
                                String phoneNumber = jsonObject.getString("PHONE_NUMBER");
                                Log.i("**************", empName + ", " + goWorkType + ", " + goWorkTime + ", " + phoneNumber);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("empName", empName);
                                map.put("goWorkTime", goWorkTime.substring(0, 19));
                                map.put("goWorkType", "1".equals(goWorkType)?"출근":"퇴근");
                                map.put("phoneNumber", phoneNumber);
                                workList.add(map);
                            }

                            SimpleAdapter simpleAdapter = new SimpleAdapter(ListActivity.this, workList, R.layout.item_list, new String[]{"empName", "goWorkTime", "goWorkType", "phoneNumber"},new int[]{R.id.name, R.id.workTime, R.id.workType, R.id.phoneNumber});
                            vWorkList.setAdapter(simpleAdapter);
                        }catch(JSONException je){
                            je.printStackTrace();
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        log("onDestroy");
        super.onDestroy();
        NuriBeacon.ListActivity = null;
    }

    private void log(String string) {Log.i("ListActivity", "*************** "+string);}
}
